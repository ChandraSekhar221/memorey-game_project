const gameContainer = document.getElementById("game");

let gifArray = Array(24).fill().map((_, index) => index + 1)

// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want ot research more
function shuffle(array) {
  let counter = array.length;

  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter);

    // Decrease counter by 1
    counter--;
    // And swap the last element with it
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}

gifArray = shuffle(gifArray)

const gifElementsArray = []
function createDivsForGifs(arr) {
  for (let index = 0; index < arr.length; index++) {
    let value = arr[index]

    let gifEl = document.createElement('div')
    gifEl.id = value
    gifEl.classList.add('gameCard')
    gifEl.style.backgroundImage = 'url(clickme.png)'
    gifEl.style.backgroundSize = 'cover'
    gifEl.style.backgroundPosition = 'center'
    gifEl.addEventListener('click', handleCardClick)

    gifElementsArray.push(gifEl)
    gameContainer.append(gifEl)
  }
}

let matchedParisIdArray = []
let clickTargetElementsArray = []

function handleCardClick(event) {
  event.target.removeEventListener('click', handleCardClick)
  let countEl = document.querySelector('#score')
  countEl.textContent = parseFloat(countEl.textContent) + 1
  let divElId = event.target.id
  if (divElId > 12) {
    divElId -= 12
  }
  let gifPath = `url(./gifs/${divElId}.gif)`
  event.target.style.backgroundImage = gifPath
  event.target.style.backgroundSize = 'cover'
  event.target.style.backgroundPosition = 'center'
  clickTargetElementsArray.push(event.target)
  filpCheck()
}

function filpCheck() {
  if (clickTargetElementsArray.length == 2) {
    let id1 = clickTargetElementsArray[0].id
    let id2 = clickTargetElementsArray[1].id

    if (id1 > 12) {
      id1 -= 12
    }
    if (id2 > 12) {
      id2 -= 12
    }
    if (id1 == id2) {
      matchedParisIdArray.push(clickTargetElementsArray[0].id)
      matchedParisIdArray.push(clickTargetElementsArray[1].id)
      clickTargetElementsArray = []
    } else {
      for (let eachGif of gifElementsArray) {
        if (!matchedParisIdArray.includes(eachGif.id)) {
          eachGif.removeEventListener('click', handleCardClick)
        }
      }
      setTimeout(() => {
        clickTargetElementsArray[0].style.background = 'url(clickme.png)'
        clickTargetElementsArray[0].style.backgroundSize = 'cover'
        clickTargetElementsArray[0].style.backgroundPosition = 'center'
        clickTargetElementsArray[1].style.background = 'url(clickme.png)'
        clickTargetElementsArray[1].style.backgroundSize = 'cover'
        clickTargetElementsArray[1].style.backgroundPosition = 'center'
        clickTargetElementsArray = []
        for (let eachGif of gifElementsArray) {
          if (!matchedParisIdArray.includes(eachGif.id)) {
            eachGif.addEventListener('click', handleCardClick)
          }
        }
      }, 800)
    }
    successMsg()
  }
}
function successMsg() {
  if (matchedParisIdArray.length == gifArray.length) {
    document.querySelector('.gameCompletedStatus').style.display = 'flex'
    let successMsgElement = document.querySelector('.successMsg')
    gameContainer.style.display = 'none'
    startGameBtnEl.style.display = 'none'
    console.log(successMsgElement)
    successMsgElement.style.display = 'block'
    let countEL = document.querySelector('#score')
    let countValue = countEL.textContent
    if (!localStorage.getItem('count')) {
      localStorage.setItem('count', countValue)
    }
    if (parseFloat(countValue) <= JSON.parse(localStorage.getItem('count'))) {
      successMsgElement.textContent = `Congratulations! You finished the Game by ${countValue} clicks.`
      localStorage.setItem('count', countValue)
    } else {
      successMsgElement.textContent = `You finished the Game by ${countValue} clicks.` + ` The best Score is ${localStorage.getItem('count')}`
    }
  }
}

document.querySelector('#best-score').textContent = localStorage.getItem('count')


let restartBtnEl = document.querySelector('.restart-game')
restartBtnEl.addEventListener('click', restart)

function restart() {
  window.location.reload()
}

// when the DOM loads
createDivsForGifs(gifArray);

function navigateToGame(event) {
  gameContainer.style.display = 'flex'
  startGameBtnEl.style.display = 'none'
  document.querySelector('.gameCompletedStatus').style.display = 'none'
}
let startGameBtnEl = document.querySelector('#main-container')
startGameBtnEl.addEventListener('click', navigateToGame)

function navigateToHome() {
  startGameBtnEl.style.display = 'flex'
  gameContainer.style.display = 'none'
  document.querySelector('.gameCompletedStatus').style.display = 'none'
}

function navigateToGameFromGameResult() {
  restart()
  navigateToHome()
}

let goRestartFromGameResultEl = document.querySelector('.restart-game-from-result')
goRestartFromGameResultEl.addEventListener('click', navigateToGameFromGameResult)
